/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author Janis
 */
public class EmpleadoEventual extends Empleado {

    private float pagoHoras;
    private float horasTrabajadas;

    public EmpleadoEventual() {
        this.pagoHoras = 0.0f;
        this.horasTrabajadas = 0.0f;
        
        
    }

    public EmpleadoEventual(float pagoHoras, float horasTrabajadas, int numEmpleado, String nombre, String puesto, String depto) {
        super(numEmpleado, nombre, puesto, depto);
        this.pagoHoras = pagoHoras;
        this.horasTrabajadas = horasTrabajadas;
    }

    public float getPagoHoras() {
        return pagoHoras;
    }

    public void setPagoHoras(float pagoHoras) {
        this.pagoHoras = pagoHoras;
    }

    public float getHorasTrabajadas() {
        return horasTrabajadas;
    }

    public void setHorasTrabajadas(float horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }
    
    
    
    
    
    
    @Override
    public float calcularPago() {
        return this.horasTrabajadas * this.pagoHoras;
    }
    
}
